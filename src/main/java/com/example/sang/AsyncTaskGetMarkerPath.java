package com.example.sang;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.sang.MapsActivityShortPath.getJSONFromAssets;
import static com.example.sang.MapsActivityShortPath.getMyLocation;
import static com.example.sang.MapsActivityShortPath.mMap;

public class AsyncTaskGetMarkerPath extends AsyncTask<String , String, JSONArray> {
    private Context context;
    public static ArrayList<Float> arrayLoc;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected JSONArray doInBackground(String... strings) {
        String stationsJsonString = getJSONFromAssets();
        try {
            JSONArray stationsJsonArray = new JSONArray(stationsJsonString);
            return stationsJsonArray;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
    public static float getDistance(LatLng latlngA, LatLng latlngB) {
        Location locationA = new Location("point A");

        locationA.setLatitude(latlngA.latitude);
        locationA.setLongitude(latlngA.longitude);

        Location locationB = new Location("point B");

        locationB.setLatitude(latlngB.latitude);
        locationB.setLongitude(latlngB.longitude);

        float distance = locationA.distanceTo(locationB)/1000;//To convert Meter in Kilometer
        return distance;
    }



    protected void onPostExecute (JSONArray result){

        String varTamponName="";
        String varTamponAdress="";
        String varTamponLat="";
        String varTamponLong="";

        if (result !=null){
            JSONObject jsonObject= null;
            float tampondist= 0;
            try {
                tampondist = getDistance(new LatLng(Double.parseDouble(result.getJSONObject(0).getString("latitude")),
                        Double.parseDouble(result.getJSONObject(0).getString("longitude"))),getMyLocation());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String blood="";
            String plasma="";
            for (int i =0; i <result.length(); i++){

                try {
                    jsonObject= result.getJSONObject(i);

                    String name=jsonObject.getString("convocationLabel");
                    String lat=jsonObject.getString("latitude");
                    String longi=jsonObject.getString("longitude");
                    String adress=jsonObject.getString("fullAddress");
                    if(jsonObject.getString("giveBlood")=="true"){
                        blood="Oui";
                    }else{
                        blood="Non";
                    }
                    if(jsonObject.getString("givePlasma")=="true"){
                        plasma="Oui";
                    }else{
                        plasma="Non";
                    }


                    float dist= getDistance(new LatLng(Double.parseDouble(lat),
                            Double.parseDouble(longi)),getMyLocation());


                    if(tampondist > dist){
                        tampondist=dist;
                        varTamponLat=lat;
                        varTamponLong=longi;
                        varTamponName=name;
                        varTamponAdress=adress;


                    }
                    Log.d("INFORMATION","MSG : "+tampondist);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            drawMarker(new LatLng(Double.parseDouble(varTamponLat),
                    Double.parseDouble(varTamponLong)), varTamponName,varTamponAdress,blood,plasma);



        }

    }
    // fucntion qui ajoute le marker du point collecte le plus proche et ma position
    private void drawMarker(LatLng point, String name,String adress,String blood, String plasma) {
        Marker markerOptions = mMap.addMarker(new MarkerOptions()
                .position(point)
                .title("Adresse: "+adress)
                .snippet(name+"\n Don du sang : "+blood+"\n Don de plasma : "+plasma)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.blood)));
        // mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        //mMap.setTrafficEnabled(true);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point,15));
/////////////////////////// my position ////////////////////////////////////////////////
        Marker markerOptions2 = mMap.addMarker(new MarkerOptions()
                .position(getMyLocation())
                .title("Adresse: Mon adresse")
                .snippet("Ma maison"));
        // mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        mMap.getUiSettings().setZoomControlsEnabled(true);
       // mMap.setTrafficEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(getMyLocation(),15));


    }



}


