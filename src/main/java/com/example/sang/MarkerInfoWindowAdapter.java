package com.example.sang;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

//classe permettant de personnaliser les bulles d'informations des marker sur maps
public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private Context context;
    public MarkerInfoWindowAdapter(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override
    public View getInfoWindow(Marker arg0) {
        return null;
    }

    @Override
    public View getInfoContents(Marker arg0) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v =  inflater.inflate(R.layout.map_marker_info_window, null);

        String title = arg0.getTitle();
        String title2 = arg0.getSnippet();
        TextView info = (TextView) v.findViewById(R.id.info);
        TextView info2 = (TextView) v.findViewById(R.id.info2);
        info.setText(title.toString());
        info2.setText(title2.toString());
        return v;
    }
}