package com.example.sang;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    public static GoogleMap mMap;
    private static final String LOG_TAG = "ExampleApp";
    public static String cityName;
    public static String latitude;
    public static String longitude;
    public static Bundle b;
    private static  final int LOCATION_REQUEST=500;
    public static Context context;
    public static double lat;
    public static double lng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        b = getIntent().getExtras();


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    //fucntion de connection a l'API pour recuperer les point de collecte en focntion d'une ville donner
    public static String getJSONFromAssets() {
        HttpURLConnection conn = null;
        final StringBuilder json = new StringBuilder();

        try {
            // Connect to the web service

            cityName = b.getString("ville");
           /* latitude=String.valueOf(loc.latitude);
            longitude=String.valueOf(loc.longitude);*/
            URL url = new URL("https://api.efs.sante.fr/carto-api/v2/SamplingLocation/SearchByCityName?CityName=" + cityName + "&UserLatitude=1.48&UserLongitude=43.5");
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Read the JSON data into the StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                json.append(buff, 0, read);
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to service", e);
            //throw new IOException("Error connecting to service", e); //uncaught
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        return json.toString();
    }

    @Override //creation de la maps
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        new AsyncTaskGetMareker().execute();

        // Setting a custom info window adapter for the google map
        MarkerInfoWindowAdapter markerInfoWindowAdapter = new MarkerInfoWindowAdapter(getApplicationContext());
        googleMap.setInfoWindowAdapter(markerInfoWindowAdapter);

        // Adding and showing marker when the map is touched
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng arg0) {
                mMap.clear();
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(arg0);
                mMap.animateCamera(CameraUpdateFactory.newLatLng(arg0));
                Marker marker = mMap.addMarker(markerOptions);
                marker.showInfoWindow();
            }
        });

    }




}



