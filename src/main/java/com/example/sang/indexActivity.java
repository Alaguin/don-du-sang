package com.example.sang;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class indexActivity extends AppCompatActivity {

    private EditText editAdress;
    private EditText editAdress2;
    private Button btnSub;
    private Button btnSub2;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.index);
            editAdress=findViewById(R.id.address);

            btnSub=findViewById(R.id.valider); // button pour la premiére fonctionnalité la recherhce de tout les point de collectes
            btnSub2=findViewById(R.id.valider2);// button de la 2éme focntionnalité la recherche du point le plus proche

            btnSub.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //on creer une nouvelle intent on definit la class de depart ici this et la class d'arrivé ici MapsActivity
                    Intent intent=new Intent(indexActivity.this,MapsActivity.class);
                    //on lance l'intent, cela a pour effet de stoper l'activité courante et lancer une autre activite ici MapsActivity
                    
                    Bundle b= new Bundle();
                    b.putString("ville",editAdress.getText().toString());
                    intent.putExtras(b);
                    startActivity(intent);
                }
            });
            btnSub2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //on creer une nouvelle intent on definit la class de depart ici this et la class d'arrivé ici MapsActivityShortPath
                    Intent intentLoc=new Intent(indexActivity.this,MapsActivityShortPath.class);
                    startActivity(intentLoc);
                }
            });
        }







}
