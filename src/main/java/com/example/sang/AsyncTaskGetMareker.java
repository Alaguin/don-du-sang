package com.example.sang;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.example.sang.MapsActivity.getJSONFromAssets;
import static com.example.sang.MapsActivity.mMap;

public class AsyncTaskGetMareker extends AsyncTask<String , String, JSONArray> {
    private Context context;
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected JSONArray doInBackground(String... strings) {
        String stationsJsonString = getJSONFromAssets();
        try {
            JSONArray stationsJsonArray = new JSONArray(stationsJsonString); // traitemeent du json contenant les point de collectes
            return stationsJsonArray;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
    // function qui recupére les données qu'on a besoin pour laffichage des marker sur maps
    protected void onPostExecute (JSONArray result){
        if (result !=null){
            for (int i =0; i <result.length(); i++){
                JSONObject jsonObject= null;
                try {
                    jsonObject= result.getJSONObject(i);
                    String blood;
                    String plasma;
                    String name=jsonObject.getString("convocationLabel");
                    String lat=jsonObject.getString("latitude");
                    String lang=jsonObject.getString("longitude");
                    String adress=jsonObject.getString("fullAddress");
                    if(jsonObject.getString("giveBlood")=="true"){
                        blood="Oui";
                    }else{
                        blood="Non";
                    }
                    if(jsonObject.getString("givePlasma")=="true"){
                        plasma="Oui";
                    }else{
                        plasma="Non";
                    }
                    drawMarker(new LatLng(Double.parseDouble(lat),
                            Double.parseDouble(lang)), name,adress,blood,plasma);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    // fucntion d'ajouter des marker de point de collecte sur la maps
    private void drawMarker(LatLng point, String name,String adress,String blood, String plasma) {
        Marker markerOptions = mMap.addMarker(new MarkerOptions()
                .position(point)
                .title("Adresse: "+adress)
                .snippet(name+"\n Don du sang : "+blood+" \nDon de plasma : "+plasma)
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.blood)));
       // mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        //mMap.setTrafficEnabled(true);
       /* markerOptions.position(point);
        markerOptions.title(name);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.blood));*/
      //  markerOptions.showInfoWindow();
       // mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point,12));

    }


}
